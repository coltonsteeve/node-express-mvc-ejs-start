/** 
*  Account model
*  Describes the characteristics of each attribute in an account resource.
*
* @author Randall Porter <s524409@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  email: { type: String, required: true, unique: true },
  username: {type: String, required: true, unique: true},
  password: {type: String, required: true, unique: false}
})

module.exports = mongoose.model('Account', AccountSchema)
