/** 
*  Account model
*  Describes the characteristics of each attribute in an account resource.
*
* @author James Runyon <s530258@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const TransactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  transactionID: { type: Number, required: true },
  cost: { type: Number, required: true },
  itemID: { type: Number, required: true },
  paymentType: { type: String, enum: ['NA', 'credit card', 'cash', 'check'], required: true, default: 'NA' },
  numberOfItem: { type: Number, required: true, default: 1 }
})

module.exports = mongoose.model('Transaction', TransactionSchema)