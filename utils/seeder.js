// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')

// require each data file

const users = require('../data/users.json')
const accounts = require('../data/accounts.json')
const transactions = require('../data/transactions.json')

// inject the app to seed the data

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}

  // Accounts don't depend on anything else...................
  db.users = new Datastore()
  db.users.loadDatabase()
  // insert the sample data into our data store
  db.users.insert(users)
  // initialize app.locals (these objects will be available to our controllers)
  app.locals.users = db.users.find(users)
  LOG.debug(`${app.locals.users.query.length} users seeded`)

  // Accounts don't depend on anything else...................
  db.accounts = new Datastore()
  db.accounts.loadDatabase()
  // insert the sample data into our data store
  db.accounts.insert(accounts)
  // initialize app.locals (these objects will be available to our controllers)
  app.locals.accounts = db.accounts.find(accounts)
  LOG.debug(`${app.locals.accounts.query.length} accounts seeded`)

   // transactions don't depend on anything else...................
   db.transactions = new Datastore()
   db.transactions.loadDatabase()
   // insert the sample data into our data store
   db.transactions.insert(transactions)
   // initialize app.locals (these objects will be available to our controllers)
   app.locals.transactions = db.transactions.find(transactions)
   LOG.debug(`${app.locals.transactions.query.length} transactions seeded`)

}
